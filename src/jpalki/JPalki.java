/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpalki;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fork
 */
public class JPalki {

    static String[]  input = {"\\//","//\\","/\\\\","///"};
    static int m=input[0].length();
    static int n=input.length;
    static char slash1='\\';
    static char slash2='/';
    static List<Point> edges = new ArrayList<>();
    static List<Point> lines = new ArrayList<>();
    static Set<String> paths=new HashSet();
    static int pathCount=0;

    public static void main(String[] args) {
        fillLines();
            //System.out.println(edges);
        while(edges.size()>0){
            for(Point p : edges){
                if(p.x<0) continue;
                findPath(p,p.y,","+p.x+","+p.y+",",p.x);
            }
            if(paths.size()==0) break;
            String minPath=null;
            int min = Integer.MAX_VALUE;
            for(String path : paths){
                if(path.length()<min){
                    minPath=path;
                    min=minPath.length();
                }
            }
            System.out.println(minPath);
            pathCount++;
            Object[] points = splitPath(minPath);
            for(Object p : points){
                for(Point e : edges){
                    if((e.x==((Point)p).x && e.y==((Point)p).y)||(e.x==((Point)p).y && e.y==((Point)p).x)){
                        edges.remove(e);
                        break;
                    }
                }
            }
//            System.out.println(edges);
//            System.out.println(lines);
            paths.clear();
        }
        
        System.out.println(pathCount);
    }
    
    private static Object[] splitPath(String path){
        path=path.substring(1);
        String[] dots = path.split(",");
        List<Point> points = new ArrayList();
        for(int i=1;i<dots.length;i++){
            points.add(new Point(Integer.parseInt(dots[i-1]),Integer.parseInt(dots[i])));
        }
        return points.toArray();
    }

        private static void fillLines(){
        for(int x=0;x<m;x++){
            edges.add(new Point(x,x+1));
            edges.add(new Point((m+1)*n+x,(m+1)*n+x+1));
        }
        for(int y=0;y<n;y++){
            edges.add(new Point(y*(m+1),(y+1)*(m+1)));
            edges.add(new Point(y*(m+1)+m,(y+1)*(m+1)+m));
        }
        for(int y=0;y<n;y++){
            for(int x=0;x<m;x++){
                char s = input[y].charAt(x);
                if(s==slash1) lines.add(new Point(y*(m+1)+x,(y+1)*(m+1)+x+1));
                else lines.add(new Point(y*(m+1)+x+1,(y+1)*(m+1)+x));
            }
        }
    }
    
    private static boolean findPath(Point last,int y,String path,int start){
        for(Point p: lines){
            if(p==last) continue;
            if(p.x==y||p.y==y){
                if(p.x==start||p.y==start) {
                    String sp=path+(p.x==y?p.y:p.x)+",";
                    paths.add(sp);
                    return true;
                    
                } else if((p.x==y && !path.contains(","+p.y+","))
                        ||(p.y==y && !path.contains(","+p.x+",")))
                        findPath(p,p.x==y?p.y:p.x,path+(p.x==y?p.y:p.x)+",",start);
            }
        };
        for(Point p: edges){
            if(p==last) continue;
            if(p.x==y||p.y==y){
                if(p.x==start||p.y==start) {
                    String sp=path+(p.x==y?p.y:p.x)+",";
                    paths.add(sp);
                    return true;
                } else if((p.x==y && !path.contains(","+p.y+","))
                        ||(p.y==y && !path.contains(","+p.x+",")))
                        findPath(p,p.x==y?p.y:p.x,path+(p.x==y?p.y:p.x)+",",start);
                
            }
        }
        return false;
    }

}
