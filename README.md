Palki!

Given a two dimensional array of size m x n consisting of forward and backward slashes, can you write an algorithm to count the number of regions where the boundaries are defined by the edges of the array and the slashes? As an example -

Given the input below, the output of your algorithm would be 6.

[
    [ \ , / , / ],
    [ / , / , \ ],
    [ / , \ , \ ]
]

![palki.jpg](https://bitbucket.org/repo/aa5bBR/images/3934326420-palki.jpg)